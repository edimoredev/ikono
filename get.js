// se realiza el javascript para leer el Json 
//se ingresa la url del Json que se quiere obtener los datos
fetch('https://demo4010019.mockable.io/products')
	.then(response => response.json())
	.then(data => {
	//  Comprobando que los datos se muestran en consola de chrome.
		console.log(data);
	//Se realiza el ciclo para encontrar todos los datos en el archivo Json.
		for (var i = 0 ; i <= data.length; i++) {
	        //Recorre hasta el ultimo dato y lo asigna a la variable.
	        //son variables fuera de la modal
	          	var idNombre = ("nombre-"+ i.toString());
	          	var idPrecio = ("price-"+ i.toString());
	          	var idImagen = ("imagen-"+ i.toString());
	        // Son variables de la modal
	          	var idNombreMod = ("nombre--"+ i.toString());
	          	var idPrecioMod = ("price--"+ i.toString());
	          	var idImagenMod = ("imagen--"+ i.toString());
	          	var idUnidadMod = ("unidad--"+ i.toString());
			 //el document.getElementById asigna la información encontrada en data[i], con la información traida en este caso es la imagen, el nombre y el precio.
	          	document.getElementById(idImagen).src = data[i].url; 
	          	document.getElementById(idNombre).innerHTML = data[i].name;
	          	document.getElementById(idPrecio).innerHTML = data[i].price;
	          	document.getElementById(idImagenMod).src = data[i].url; 
	          	document.getElementById(idNombreMod).innerHTML = data[i].name;
	          	document.getElementById(idPrecioMod).innerHTML = data[i].price;
	          	document.getElementById(idUnidadMod).innerHTML = data[i].unit;        
		     }
		    })
		    .catch(err => {
		        console.error('An error ocurred', err);
		    });